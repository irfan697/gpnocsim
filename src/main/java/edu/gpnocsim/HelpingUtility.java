package edu.gpnocsim;

import java.util.*;
import java.io.*;

/**
 * HelpingUtility class defines the methods for generating random values with
 * uniform distribution using {@link java.util.Random}.
 * <p>
 * HelpingUtility class also reads the input parameters and their associated
 * values from the input configuration file.
 * <p>
 * Conversion of the clock speed to appropriate speed by a factor is also
 * performed in this class.
 *
 */
public class HelpingUtility {
	/** a member of the java.Util.Random class */
	private Random rand = null;

	/** list of input parameters */
	private Vector allParamSet = null;

	/**
	 * Constructor method. Performs initialization of the class variables.
	 */
	public HelpingUtility() {
		rand = new Random(12345);
		allParamSet = new Vector();
	}

	/**
	 * Sets the seed for the randomizer
	 *
	 * @param seed
	 *            seed value
	 */
	public void setRandSeed(int seed) {
		rand = new Random(seed);
	}

	/**
	 * Instantiates the rand variable.
	 *
	 */
	public void setRandomSeed() {
		rand = new Random();
	}

	/**
	 * Returns a random number from 0 to 1 using Uniform Distribution.
	 *
	 * @return random value from 0 to 1
	 */
	public double getNextRandomNumber() {
		return rand.nextDouble();
	}

	/**
	 * Returns a set of input parameters for a particular network speicied by
	 * the arguement of the method.
	 *
	 * @param index
	 *            Index of the Network to be tested
	 * @return a set of input parameters
	 */
	public Vector getParamSet(int index) {
		if (index < allParamSet.size())
			return (Vector) allParamSet.get(index);

		return null;
	}

	/**
	 * Reads the input configuration file. Each empty line defines a new set of
	 * input parameters. Typically a set of input parameters correspond to the
	 * simulation for a particular topology or configuration.
	 * <p>
	 * A typical line of the file is of the format <parameter name>=<parameter
	 * value>. The parameter name and its value is stored in the
	 * {@link ParamDTO} object.
	 *
	 * @param parameterFile
	 *            the name of the input configuration file
	 * @see BufferedReader
	 * @see StringTokenizer
	 */
	public void readParameterFromFile(String parameterFile) {

		StringTokenizer theTokenizer;
		String parameter, value;
		Vector paramSet = new Vector();
		try {
			InputStream paramStream = ClassLoader.getSystemResourceAsStream(parameterFile);

			BufferedReader paramReader = new BufferedReader(new InputStreamReader(paramStream));

			String paramLine = paramReader.readLine();

			while (paramLine != null) {
				if (paramLine.equalsIgnoreCase("")) {
					allParamSet.add(paramSet);
					paramSet = new Vector();
					System.out.println("Added: ");

				} else {

					theTokenizer = new StringTokenizer(paramLine, " =;,:",
							false);
					if (theTokenizer.countTokens() >= 2) {
						parameter = theTokenizer.nextToken();
						value = theTokenizer.nextToken();
						paramSet.add(new ParamDTO(parameter, value));
					}
				}
				paramLine = paramReader.readLine();
				System.out.println("ReadLine: " + paramLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a converted cycle for the factor specified in the arguement.
	 *
	 * @param cycle
	 *            cycle value of an entity, for example a resource
	 * @param factor
	 *            ratio of the speed between an entity, for example a resource
	 *            and the switch
	 * @return value the cycle with respect to the switch
	 */

	public int getConvertedCycle(int cycle, double factor) {
		return (int) Math.floor((cycle) * factor);
	}

	public boolean validateMcrNodeCount(int count){

		// the node count should be such that it results in an Interger number
		// for the ring count
		if(count % IConstants.MCR_SWITCHES_PER_RING != 0 ) return false;

		return true;
	}

	/**
	 *
	 * @param integer
	 * @return
	 */
	public static int getRequiredBitCount(int integer){

		// using log to the base 2
		int base = 2;
		if(integer == 1) return 1;
		double result = Math.log(integer) / Math.log(base);
		result = Math.ceil(result);

		return (int) result;
	}

	/** Returns the type of the Ring depending on its position in the MCR
	 * network. The indexing has been assumed in two dimensions (rows and columns)
	 * increasing from left to right and from top to bottom.
	 *
	 * @param row
	 * @param col
	 * @return
	 */
	public MCRRingType getMCRRingType(int row, int col){

		int minRowIndex = 0;
		int minColIndex = 0;

		int maxRowIndex = IConstants.MCR_ROWS - 1;  // zero up to n-1
		int maxColIndes = IConstants.MCR_COLS - 1;

		if(row == minRowIndex && col == minColIndex)
			return MCRRingType.TOP_LEFT_CORNER;

		if(row == minRowIndex && col == maxColIndes)
			return MCRRingType.TOP_RIGHT_CORNER;

		if(row == minRowIndex)
			return MCRRingType.TOP_ROW;  // two possible corners already checked

		if(row == maxRowIndex && col == minColIndex)
			return MCRRingType.BOTTOM_LEFT_CORNER;

		if(row == maxRowIndex && col == maxColIndes)
			return MCRRingType.BOTTOM_RIGHT_CORNER;

		if(row == maxRowIndex)
			return MCRRingType.BOTTOM_ROW;  // two possible corners already checked

		if(col == minColIndex)
			return MCRRingType.LEFT_MOST_COLUMN;  // because it cannot be a corner now

		if(col == maxColIndes)
			return MCRRingType.RIGHT_MOST_COLUMN;  // because it cannot be a corner now

		return MCRRingType.OTHER;
	}

}
