package edu.gpnocsim;

/**
 * <p>
 * The MCRRouter class is the implementation of the Router interface for the
 * Mesh Connect Ring Network.
 * </p>
 *
 *
 * @version 1.0
 *
 */

public class MCRRouter implements Router {

	/**
	 * <p>
	 * Implementation of the determineRoute() method of the Router interface.
	 * Returns which path the flit passed be transmitted to ultimately reach the
	 * destination.
	 * </p>
	 *
	 *
	 * @param source
	 *            address of the source node
	 * @param dest
	 *            address of the destination node
	 * @param switchAddr
	 *            address of the switch where the routing is taking place
	 *
	 * @return output link number of the switch
	 */
	public int determineRoute(int source, int dest, int switchAddr) {
		return mcrStaticRoute(source, dest, switchAddr);
	}

	/**
	 *
	 * @param source
	 * @param dest
	 * @param switchAddr
	 * @return output link number
	 */

	private int mcrStaticRoute(int source, int dest, int currentSwitchAddress) {
	    // The bit representation of the address field from left to right is
	    // rowIndex-columnIndex-positionInTheRing-NodeBits

	    // we start deciphering each component of the address from right to left
	    int temp = currentSwitchAddress;

//	    // node bits are the same for mesh, torus and MCR networks
//	    int currentNodeAddress = getAddressComponent(temp, IConstants.MESH_NODE_BITS_REQ);
//	    // discard the used portion
//	    temp = temp >> IConstants.MESH_NODE_BITS_REQ;

	    int currentRingPosition = getAddressComponent(temp, IConstants.MCR_SWITCHES_PER_RING_BITS);
	    temp = temp >> IConstants.MCR_SWITCHES_PER_RING_BITS;

	    int currentColumn = getAddressComponent(temp, IConstants.MCR_COL_BITS);
	    temp = temp >> IConstants.MCR_COL_BITS;

	    int currentRow = getAddressComponent(temp, IConstants.MCR_ROW_BITS);
	    temp = temp >> IConstants.MCR_ROW_BITS;

	    // doing the same for the destination address
	    temp = dest;
	    // node bits are the same for mesh, torus and MCR networks
        int destNodeAddress = getAddressComponent(temp, IConstants.MESH_NODE_BITS_REQ);
        // discard the used portion
        temp = temp >> IConstants.MESH_NODE_BITS_REQ;

        int destRingPosition = getAddressComponent(temp, IConstants.MCR_SWITCHES_PER_RING_BITS);
        temp = temp >> IConstants.MCR_SWITCHES_PER_RING_BITS;

        int destColumn = getAddressComponent(temp, IConstants.MCR_COL_BITS);
        temp = temp >> IConstants.MCR_COL_BITS;

        int destRow = getAddressComponent(temp, IConstants.MCR_ROW_BITS);
        temp = temp >> IConstants.MCR_ROW_BITS;

        int rowOffset = destRow - currentRow;
        int columnOffset = destColumn - currentColumn;
        int ringPositionOffset = destRingPosition ^ currentRingPosition;

        /*
         *  The routing algorithm suggested in the research paper
         *
         *  "A Mesh-Connected Rings Topology for Network-on-Chip"
         *
         *  Presented in: 2012 13th International Conference on Parallel and
         *  Distributed Computing, Applications and Technologies
         *
         *  LIU Youyao (lyyao2002@xupt.edu)
         *  HAN Jungang (hjg@xupt.edu)
         */

        // using variable names similar to the research paper, to make it comparable
        int m = MCRSwitch.ADJACENT_RING_NEIGHBOR_INDEX;
        int c0 = MCRSwitch.COUNTER_CLOCK_NEIGHBOR_INDEX;
        int c1 = MCRSwitch.CLOCKWISE_NEIGHBOR_INDEX;

        int linkIndex = -1; // invalid index, indicates that we've reached the destination

        if(rowOffset < 0){

            if(currentRingPosition == 0){
                linkIndex = m;
            } else if(currentRingPosition == 3){
                linkIndex = c1;
            } else {
                linkIndex = c0;
            }
        }

        if(rowOffset > 0){
            if(currentRingPosition == 2){
                linkIndex = m;
            } else if(currentRingPosition == 1){
                linkIndex = c1;
            } else {
                linkIndex = c0;
            }
        }

        if(columnOffset < 0){

            if(currentRingPosition == 3){
                linkIndex = m;
            } else if(currentRingPosition == 0){
                linkIndex = c0;
            } else {
                linkIndex = c1;
            }
        }

        if(columnOffset > 0){

            if(currentRingPosition == 1){
                linkIndex = m;
            } else if(currentRingPosition == 2){
                linkIndex = c0;
            } else {
                linkIndex = c1;
            }

        }

        if(rowOffset == 0 && columnOffset == 0){
            if(ringPositionOffset == 1){

                if(currentRingPosition == 0 || currentRingPosition == 2){
                    linkIndex = c1;
                } else {
                    linkIndex = c0;
                }

            } else if(ringPositionOffset == 2){

                if(currentRingPosition == 1 || currentRingPosition == 3){
                    linkIndex = c1;
                } else {
                    linkIndex = c0;
                }

            } else if(ringPositionOffset == 3){

                if(currentRingPosition == 0 || currentRingPosition == 2){
                    linkIndex = c1;
                } else {
                    linkIndex = c0;
                }

            }
        }

        // which physical link should of the switch was selected for forwarding
        int pLinkForForwarding;
        if(linkIndex == -1){
            // The packet reached the destination
            // This results in physical link = 0, which means the packet has
            // reached
            pLinkForForwarding = dest - (currentSwitchAddress << IConstants.MESH_NODE_BITS_REQ);
        }
        else {
            // to accommodate the physical links for the nodes connected to each switch
            pLinkForForwarding = linkIndex + IConstants.MESH_ADJ_NODE;
        }

        System.out.println("Physical link index decided: " + pLinkForForwarding + "\n");
        return pLinkForForwarding;

	}

	private static int getAddressComponent(int address, int bitsToShift){
	    // strip off the rightmost n number of bits, where n = bitsToShift
	    int shiftedAddress = address >> bitsToShift;
	    // shift back to create an equal number of trailing 0's
	    shiftedAddress = shiftedAddress << bitsToShift;

	    return address - shiftedAddress;
	}

}
