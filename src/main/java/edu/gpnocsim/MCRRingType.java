package edu.gpnocsim;

public enum MCRRingType {
	TOP_LEFT_CORNER,
	TOP_RIGHT_CORNER,
	BOTTOM_LEFT_CORNER,
	BOTTOM_RIGHT_CORNER,

	TOP_ROW,
	BOTTOM_ROW,
	LEFT_MOST_COLUMN,
	RIGHT_MOST_COLUMN,

	OTHER // any ring other than the above mentioned types
}
