package edu.gpnocsim;

public class MCRRing {


	/** It is essentially an array of 4 MCR switches which constitute a Ring
	 * in the MCR network.
	 *
	 */
	private final MCRSwitch switches[];

	/** A 32-bit int field used to represent the address of the MCR ring in
	 * the network. The address constitutes row-bits followed by the column-bits
	 * from left to right. The redundant bits are to the left of the address
	 * bits, as usual
	 *
	 * @see IConstants#MCR_ROW_BITS
	 * @see IConstants#MCR_COL_BITS
	 *
	 *
	 */
	private final int mcrRingAddress;


	private static final int UPPER_SWITCH_INDEX = 0;
	private static final int RIGHT_SWITCH_INDEX = 1;
	private static final int LOWER_SWITCH_INDEX = 2;
	private static final int LEFT_SWITCH_INDEX = 3;

	/** MCR ring is represented along with its address. Also, the four switches
	 * are know about their clockwise and counter-clockwise neighbors in the
	 * MCR ring on initialization
	 *
	 * @param mcrRingAddress
	 * @param switches
	 */
	public MCRRing(int mcrRingAddress, MCRSwitch switches[]){
		if(switches.length != IConstants.MCR_SWITCHES_PER_RING)
			throw new IllegalStateException("Number of switches in the "
					+ "MCR ring must be " + IConstants.MCR_SWITCHES_PER_RING);

		this.mcrRingAddress = mcrRingAddress;
		this.switches = switches;
		connectSwitchesInRing();
	}

	/** <p>
	 * Each MCR ring has 4 switches and each switch is connected to two other
	 * switches within the same ring.
	 *</p>
	 *
	 */

	/*
	 * For every switch in the ring,
	 * index+1 is clockwise neighbor, except the last one, for which index=0 is
	 * the clockwise neighbor
	 */
	private void connectSwitchesInRing(){

		// setting clockwise neighbors for the four switches in the ring.
		for(int i = 0; i<switches.length-1; i++){ // works for switches 0,1,2
			switches[i].setClockwiseNeighbor(switches[i+1]);
		}
		// for switch[3]
		switches[switches.length-1].setClockwiseNeighbor(switches[0]);

		// setting counter-clockwise neighbors for the four switches
		for(int i = switches.length-1; i > 0; i--){ // works for switches 3,2,1
			switches[i].setCounterClockwiseNeighbor(switches[i-1]);
		}
		// for switch[0]
		switches[0].setCounterClockwiseNeighbor(switches[switches.length-1]);
	}

	/** Connects the {@code mcrSwitch} to the upper switch of the ring
	 * @param mcrSwitch
	 */
	public void connectToUpperSwitch(MCRSwitch mcrSwitch){
		switches[UPPER_SWITCH_INDEX].setAdjacentRingNeighbor(mcrSwitch);
	}

	/** Connects the {@code mcrSwitch} to the right switch of the ring
	 * @param mcrSwitch
	 */
	public void connectToRightSwitch(MCRSwitch mcrSwitch){
		switches[RIGHT_SWITCH_INDEX].setAdjacentRingNeighbor(mcrSwitch);
	}

	/** Connects the {@code mcrSwitch} to the lower switch of the ring
	 * @param mcrSwitch
	 */
	public void connectToLowerSwitch(MCRSwitch mcrSwitch){
		switches[LOWER_SWITCH_INDEX].setAdjacentRingNeighbor(mcrSwitch);;
	}

	/** Connects the {@code mcrSwitch} to the left switch of the ring
	 * @param mcrSwitch
	 */
	public void connectToLeftSwitch(MCRSwitch mcrSwitch){
		switches[LEFT_SWITCH_INDEX].setAdjacentRingNeighbor(mcrSwitch);
	}

	/** @return a reference to the upper switch in the MCR ring
	 */
	public MCRSwitch getUpperSwitch(){
		return switches[UPPER_SWITCH_INDEX];
	}

	/** @return a reference to the right switch in the MCR ring
	 */
	public MCRSwitch getRightSwitch(){
		return switches[RIGHT_SWITCH_INDEX];
	}

	/** @return a reference to the lower switch in the MCR ring
	 */
	public MCRSwitch getLowerSwitch(){
		return switches[LOWER_SWITCH_INDEX];
	}

	/** @return a reference to the left switch in the MCR ring
	 */
	public MCRSwitch getLeftSwitch(){
		return switches[LEFT_SWITCH_INDEX];
	}

	public MCRSwitch [] getSwitches(){
		return switches;
	}

	public int getMcrRingAddress(){
		return mcrRingAddress;
	}
}
